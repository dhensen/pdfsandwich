FROM phusion/baseimage:master

RUN apt-get update && apt-get install -y \
    wget \
    unpaper \
    exactimage \
    imagemagick \
    poppler-utils \
    ghostscript \
    tesseract-ocr \
    tesseract-ocr-nld
RUN wget https://sourceforge.net/projects/pdfsandwich/files/pdfsandwich%200.1.7/pdfsandwich_0.1.7_amd64.deb
RUN dpkg -i pdfsandwich_0.1.7_amd64.deb

COPY policy.xml /etc/ImageMagick-6/policy.xml

VOLUME ["/data"]
WORKDIR /data

ENTRYPOINT ["/usr/bin/pdfsandwich"]

CMD ["--help"]
